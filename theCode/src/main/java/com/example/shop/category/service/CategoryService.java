package com.example.shop.category.service;

import com.example.shop.category.controller.CategoryModel;
import com.example.shop.category.controller.CategoryModelChildTree;
import com.example.shop.category.controller.CategoryModelCreate;
import com.example.shop.category.controller.CategoryModelUpdate;
import com.example.shop.category.persistence.Category;

import java.util.List;
import java.util.Set;

public interface CategoryService {

    CategoryModel create(CategoryModelCreate categoryModelCreate);


    CategoryModel show(Integer id);

    CategoryModel update(CategoryModelUpdate categoryModelUpdate);

    void delete(Integer id);

    List<CategoryModel> index();

    public Category getCategory(Integer id);

    Set<Category> getSomeCategories(List<Integer> categories_id);

    List<CategoryModelChildTree> tree();


}
