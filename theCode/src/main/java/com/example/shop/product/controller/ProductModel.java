package com.example.shop.product.controller;

import com.example.shop.category.controller.CategoryModel;
import com.example.shop.category.controller.CategoryModelProduct;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ProductModel {
    private Integer id;
    private String name;
    private String description;
    private String pricePerUnit;
    private Integer quantity;
    private List<CategoryModelProduct> categories = new ArrayList<>();
    private List<ImageModelProduct> images = new ArrayList<>();

}
