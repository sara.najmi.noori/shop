package com.example.shop.product.controller;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
public class ProductModelCreate {
    @NotBlank(message = "Please enter the name of product: ")
    private String name;
    @NotBlank(message = "Please write a description for product: ")
    private String description;
    @NotBlank(message = "Please enter the price of product per unit: ")
    private String pricePerUnit;
    @NotBlank(message = "Please enter the quantity of the product: ")
    private Integer quantity;
    @NotBlank(message = "Please enter the categories of the product: ")
    private List<Integer> categories_id;
//    @NotBlank(message = "Please enter the images of the product: ")
//    private List<Integer> images_id;

}
