package com.example.shop.category.controller;

import lombok.Data;

@Data
public class CategoryModelProduct {
    private Integer id;
    private String name;
}
