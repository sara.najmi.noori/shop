package com.example.shop.category.persistence;

import com.example.shop.base.config.RunConfiguration;
import com.example.shop.base.config.SchemaConfig;
import com.example.shop.product.persistence.Product;
import lombok.Data;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Data
@Table(name = "categories",schema = RunConfiguration.DB)
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name",nullable = false)
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_id",referencedColumnName = "id")
    private Category parent;

    @OneToMany(mappedBy = "parent",fetch =FetchType.LAZY)
    private Collection<Category> children;

    @ManyToMany(mappedBy = "categories",fetch = FetchType.LAZY)
    private Collection<Product> products;


}
