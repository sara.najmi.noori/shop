package com.example.shop.category.service;

import com.example.shop.base.ResourceNotFoundException;
import com.example.shop.category.controller.*;
import com.example.shop.category.persistence.Category;
import com.example.shop.category.persistence.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@Transactional(rollbackOn = Exception.class)
@Qualifier("CategoryServiceImpl")
@Service
public class CategoryServiceImpl implements CategoryService {
    private CategoryRepository categoryRepository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }


    private static CategoryModel convertEntityToModel(Category category) {
        CategoryModel categoryModel =new CategoryModel();
        categoryModel.setId(category.getId());
        categoryModel.setName(category.getName());
        if (category.getParent()!=null){
            categoryModel.setParent(convertEntityToModelParent(category.getParent()));
        }
        if (category.getChildren()!=null){
            categoryModel.setChildren(category.getChildren().
                    stream().
                    map(CategoryServiceImpl::convertEntityToModelChild).
                    collect(Collectors.toList()));
        }

        return categoryModel;
    }

    private static CategoryModelParent convertEntityToModelParent(Category category) {
        CategoryModelParent categoryModelParent =new CategoryModelParent();
        categoryModelParent.setId(category.getId());
        categoryModelParent.setName(category.getName());
        return categoryModelParent;
    }

    private static CategoryModelChild convertEntityToModelChild(Category category) {
        CategoryModelChild categoryModelChild =new CategoryModelChild();
        categoryModelChild.setId(category.getId());
        categoryModelChild.setName(category.getName());
        return categoryModelChild;
    }

    private static CategoryModelChildTree convertEntityToModelTree(Category category) {
        CategoryModelChildTree categoryModelChildTree =new CategoryModelChildTree();
        categoryModelChildTree.setId(category.getId());
        categoryModelChildTree.setName(category.getName());
        categoryModelChildTree.setChild(category.getChildren()
                .stream()
                .map(CategoryServiceImpl::convertEntityToModelTree)
                .collect(Collectors.toList()));
        return categoryModelChildTree;
    }

    @Override
    public Category getCategory(Integer id) {
        return categoryRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("category", "id", id));
    }

    @Override
    public Set<Category> getSomeCategories(List<Integer> categories_id) {
        return new HashSet<>(categoryRepository.findAllById(categories_id));
    }

    @Override
    public List<CategoryModelChildTree> tree() {
        return categoryRepository.findAllByParentIsNull().stream()
                .map(CategoryServiceImpl::convertEntityToModelTree)
                .collect(Collectors.toList());
    }

    public CategoryModel create(CategoryModelCreate category) {

        Category newCategory = new Category();
        newCategory.setName(category.getName());
        if (category.getParent_id() != null) {
            newCategory.setParent(getCategory(category.getParent_id()));
        }
        categoryRepository.save(newCategory);
        return convertEntityToModel(newCategory);
    }


    @Override
    public CategoryModel show(Integer id) {
        return convertEntityToModel(getCategory(id));
    }

    @Override
    public List<CategoryModel> index() {
        return categoryRepository.findAll()
                .stream()
                .map(CategoryServiceImpl::convertEntityToModel)
                .collect(Collectors.toList());
    }

    @Override
    public CategoryModel update(CategoryModelUpdate categoryModelUpdate) {
        Category category = getCategory(categoryModelUpdate.getId());
        category.setName(categoryModelUpdate.getName());
        if (categoryModelUpdate.getParent_id() != null) {
            category.setParent(getCategory(categoryModelUpdate.getParent_id()));
        }else{
            category.setParent(null);
        }
        categoryRepository.save(category);
        return convertEntityToModel(category);
    }

    @Override
    public void delete(Integer id) {
        Category category = getCategory(id);
        categoryRepository.delete(category);
    }


}
