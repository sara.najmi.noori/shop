package com.example.shop.product.controller;

import com.sun.istack.NotNull;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
public class ProductModelUpdate {
    @NotNull
    private Integer id;
    @NotBlank(message = "Please Enter the price of product per unit: ")
    private String pricePerUnit;
    @NotBlank(message = "Please Enter the name of product per unit: ")
    private String name;
    @NotBlank(message = "Please enter the quantity of the product: ")
    private Integer quantity;
    @NotBlank(message = "Please enter the categories of the product: ")
    private List<Integer> categories_id;
//    @NotBlank(message = "Please enter the images of the product: ")
//    private List<Integer> images_id;
}
