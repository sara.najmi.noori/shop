package com.example.shop.category.controller;

import lombok.Data;

import java.util.List;

@Data
public class CategoryModel {
    private Integer id;
    private String name;
    private CategoryModelParent parent;
    private List<CategoryModelChild> children;
}
