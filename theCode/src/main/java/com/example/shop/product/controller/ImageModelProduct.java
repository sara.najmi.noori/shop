package com.example.shop.product.controller;

import lombok.Data;

@Data
public class ImageModelProduct {
    private Integer id;
    private String url;
    private Integer width;
    private Integer height;
}
