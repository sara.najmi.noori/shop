package com.example.shop.product.persistence;

import com.example.shop.base.Entity.DateAudit;
import com.example.shop.base.config.SchemaConfig;
import com.example.shop.category.persistence.Category;
import lombok.Data;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Data
@Table(name = "products",schema = SchemaConfig.DB)
public class Product extends DateAudit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name",nullable = false)
    private String name;

    @Column(name="description")
    private String description;

    @Column(name = "price_per_unit",nullable = false)
    private String pricePerUnit;

    @Column(name="quantity",nullable = false)
    private Integer quantity;

    @OneToMany(mappedBy = "product",fetch = FetchType.LAZY)
    private Collection<Image> images;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "categories_products",
            joinColumns = @JoinColumn(name = "product_id",referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "category_id",referencedColumnName = "id")
    )
    private Collection<Category> categories;

}
