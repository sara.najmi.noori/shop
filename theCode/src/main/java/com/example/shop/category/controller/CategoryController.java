package com.example.shop.category.controller;

import com.example.shop.category.persistence.Category;
import com.example.shop.category.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins="http://localhost:8081")
@RequestMapping("/category/controller")
@RestController
public class CategoryController {
    private CategoryService categoryService;
    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @PostMapping("/create")
    public ResponseEntity<CategoryModel> createCategory(@RequestBody @Valid CategoryModelCreate newCategory) {
        return ResponseEntity.ok(categoryService.create(newCategory));
    }

    @GetMapping("/index")
    public ResponseEntity<List<CategoryModel>> indexCategory() {
        return ResponseEntity.ok(categoryService.index());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Category> readCategory(@PathVariable Integer id) {
        return ResponseEntity.ok(categoryService.getCategory(id));
    }

    @PutMapping("/update")
    public ResponseEntity<CategoryModel> updateCategory(@RequestBody @Valid CategoryModelUpdate newCategory) {
        return ResponseEntity.ok(categoryService.update(newCategory));
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity deleteCategory(@PathVariable Integer id) {
        categoryService.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }

}
