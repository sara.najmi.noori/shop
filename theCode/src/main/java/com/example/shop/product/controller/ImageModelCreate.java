package com.example.shop.product.controller;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ImageModelCreate {
    @NotBlank(message = "Please enter the url of image: ")
    private String url;
    @NotBlank(message = "Please enter the width of image: ")
    private Integer width;
    @NotBlank(message = "Please enter the height of image: ")
    private Integer height;
//    @NotBlank(message = "Please enter the product of image: ")
//    private ProductModelImage product;

}
