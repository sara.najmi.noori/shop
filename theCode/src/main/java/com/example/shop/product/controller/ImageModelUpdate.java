package com.example.shop.product.controller;

import com.sun.istack.NotNull;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
public class ImageModelUpdate {
    @NotNull
    private Integer id;
    @NotBlank(message = "Please enter the url of image: ")
    private String url;
    @NotBlank(message = "Please enter the width of image: ")
    private Integer width;
    @NotBlank(message = "Please enter the height of image: ")
    private Integer height;
    @NotBlank(message = "Please enter the product id of image: ")
    private Integer product_id;
}
