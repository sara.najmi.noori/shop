package com.example.shop.category.controller;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CategoryModelUpdate {
    @NotNull
    private Integer id;
    @NotBlank(message = "Please Enter the name of category: ")
    private String name;
    private Integer parent_id;

}
