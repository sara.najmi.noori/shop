package com.example.shop.product.controller;

import com.example.shop.category.controller.CategoryModelProduct;
import lombok.Data;

import java.util.Collection;

@Data
public class ImageModel {
    private Integer id;
    private String url;
    private Integer width;
    private Integer height;
//    private ProductModelImage product;
    private Integer Photo_id;
    private String name;
    private Integer pricePerUnit;

    private Collection<CategoryModelProduct> categories;

}
