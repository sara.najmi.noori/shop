package com.example.shop.category.controller;

import lombok.Data;

@Data
public class CategoryModelParent {
    private Integer id;
    private String name;

}
