package com.example.shop.category.controller;

import lombok.Data;

import java.util.List;

@Data
public class CategoryModelChildTree {
    private Integer id;
    private String name;
    private List<CategoryModelChildTree> child;
}
