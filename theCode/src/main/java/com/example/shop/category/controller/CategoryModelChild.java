package com.example.shop.category.controller;

import lombok.Data;

@Data
public class CategoryModelChild {
    private Integer id;
    private String name;

}
