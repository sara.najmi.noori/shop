package com.example.shop.category.controller;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class CategoryModelCreate {
    @NotBlank(message = "Please enter the name of category: ")
    private String name;
    private Integer parent_id;

}
