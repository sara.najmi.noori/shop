package com.example.shop.product.persistence;

import com.example.shop.base.Entity.DateAudit;
import com.example.shop.base.config.SchemaConfig;
import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Data
@Table(name = "images",schema = SchemaConfig.DB)
public class Image extends DateAudit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "url",nullable = false)
    private String url;

    @Column(name = "width",nullable = false)
    private Integer width;

    @Column(name = "height",nullable = false)
    private Integer height;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_id",referencedColumnName = "id")
    private Product product;

}
