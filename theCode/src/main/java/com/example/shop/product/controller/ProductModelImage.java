package com.example.shop.product.controller;

import lombok.Data;

import java.util.List;

@Data
public class ProductModelImage {
    private Integer id;

    private String name;

    private String description;

    private Integer pricePerUnit;

    private List<Integer> categories_id;

}
